const allEven = (arr: number[]): number[] => {
    const result:number[]= [];
    for (const i in arr) {
      if (arr[i] % 2 === 0) {
        result.push(arr[i]);
      }
    }
    return result;
  }
  export default allEven;